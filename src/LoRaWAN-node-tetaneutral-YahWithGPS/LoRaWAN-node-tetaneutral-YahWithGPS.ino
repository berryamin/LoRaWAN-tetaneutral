#include <lmic.h>
#include <hal/hal.h>
#include <SPI.h>
#include <Wire.h>
#include <TinyGPS.h>

#define DEFAULT_LAT 43.604381
#define DEFAULT_LON 1.443366

TinyGPS gps;
float flat = DEFAULT_LAT;
float flon = DEFAULT_LON;
unsigned long age;

#define LED_RED   8
#define LED_GREEN 6
#define LED_BLUE  9
#define BUTTON_PIN 38

// keys for iot.tetaneutral.net
// static const u1_t PROGMEM M_DEVEUI[8] = { 0x80, 0x56, 0x68, 0x9c, 0xb4, 0x2b, 0x78, 0x8a };
// static const u1_t PROGMEM M_APPEUI[8] = { 0x79, 0xc4, 0xe2, 0xcc, 0xc5, 0xf3, 0xe2, 0xe9 };
// static const u1_t PROGMEM M_APPKEY[16] = { 0xa4, 0x6a, 0x07, 0xe9, 0xb0, 0x28, 0xb4, 0x8c, 0x63, 0xe0, 0x4c, 0xf4, 0xa8, 0x72, 0x51, 0x4a};

// keys for loraserver.tetaneutral.net
static const u1_t PROGMEM M_APPEUI[8] =  { 0xF5, 0xD4, 0x54, 0x4B, 0x1C, 0xAB, 0x54, 0x1C };
static const u1_t PROGMEM M_DEVEUI[8] =  { 0x4a, 0x89, 0xbc, 0x77, 0x05, 0xe2, 0x1c, 0x67 };
static const u1_t PROGMEM M_APPKEY[16] = { 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00 };

void os_getArtEui (u1_t* buf)
{
    memcpy_P(buf, M_APPEUI, 8);
}

void os_getDevEui (u1_t* buf)
{
    memcpy_P(buf, M_DEVEUI, 8);
}

void os_getDevKey (u1_t* buf)
{
    memcpy_P(buf, M_APPKEY, 16);    
}

static uint8_t mydata[20];
static osjob_t sendjob;

const unsigned TX_INTERVAL = 30;

// Yah! "Yet Another Hardware for !oT"
const lmic_pinmap lmic_pins = {
    .nss = 7,
    .rxtx = LMIC_UNUSED_PIN,
    .rst = 19,
    .dio = {31, 27, 26},
};

// Adafruit Feather
/*
const lmic_pinmap lmic_pins = {
        .nss = 8,
        .rxtx = LMIC_UNUSED_PIN,
        .rst = 4,
        .dio = {3, 6, LMIC_UNUSED_PIN},
};*/

void onEvent (ev_t ev)
{
    SerialUSB.print(os_getTime());
    SerialUSB.print(": ");
    switch(ev) {
        case EV_SCAN_TIMEOUT:
            SerialUSB.println("EV_SCAN_TIMEOUT");
            break;
        case EV_BEACON_FOUND:
            SerialUSB.println("EV_BEACON_FOUND");
            break;
        case EV_BEACON_MISSED:
            SerialUSB.println("EV_BEACON_MISSED");
            break;
        case EV_BEACON_TRACKED:
            SerialUSB.println("EV_BEACON_TRACKED");
            break;
        case EV_JOINING:
            digitalWrite(LED_RED, LOW);
            SerialUSB.println("EV_JOINING");
            break;
        case EV_JOINED:
            digitalWrite(LED_RED, HIGH);
            SerialUSB.println("EV_JOINED");
            LMIC_setLinkCheckMode(0);
            os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
            break;
        case EV_RFU1:
            SerialUSB.println("EV_RFU1");
            break;
        case EV_JOIN_FAILED:
            SerialUSB.println("EV_JOIN_FAILED");
            break;
        case EV_REJOIN_FAILED:
            SerialUSB.println("EV_REJOIN_FAILED");
            break;
            break;
        case EV_TXCOMPLETE:
            digitalWrite(LED_BLUE, HIGH);
            SerialUSB.println("EV_TXCOMPLETE (includes waiting for RX windows)");
            if (LMIC.txrxFlags & TXRX_ACK)
                SerialUSB.println("Received ack");
            if (LMIC.dataLen) {
                SerialUSB.print("Received ");
                SerialUSB.print(LMIC.dataLen);
                SerialUSB.println(" bytes of payload");
            }
            os_setTimedCallback(&sendjob, os_getTime()+sec2osticks(TX_INTERVAL), do_send);
            break;
        case EV_LOST_TSYNC:
            SerialUSB.println("EV_LOST_TSYNC");
            break;
        case EV_RESET:
            SerialUSB.println("EV_RESET");
            break;
        case EV_RXCOMPLETE:
            // data received in ping slot
            SerialUSB.println("EV_RXCOMPLETE");
            break;
        case EV_LINK_DEAD:
            SerialUSB.println("EV_LINK_DEAD");
            break;
        case EV_LINK_ALIVE:
            SerialUSB.println("EV_LINK_ALIVE");
            break;
         default:
            SerialUSB.println("Unknown event");
            break;
    }
}

void do_send(osjob_t* j)
{
    static uint32_t sqn = 0;
    String flat_s;
    String flon_s;
    flat_s = String(flat,7);
    flon_s = String(flon,7);
    
    if (LMIC.opmode & OP_TXRXPEND)
    {
        SerialUSB.println("OP_TXRXPEND, not sending");
    }
    else
    {
        digitalWrite(LED_BLUE, LOW);
        sprintf((char*)mydata,"{\"sqn\":%d,\"lat\":%s,\"lon\":%s}", sqn, flat_s.c_str(), flon_s.c_str() );
        SerialUSB.print("I will send: ");
        SerialUSB.println((char*)mydata);
        LMIC_setTxData2(1, mydata, strlen((char*)mydata), 1);
        sqn++;
    }
}

void setup()
{
    SerialUSB.begin(115200);
    Serial1.begin(9600);
    //while(!SerialUSB);
    SerialUSB.println("Starting");

    pinMode(LED_RED, OUTPUT);
    pinMode(LED_GREEN, OUTPUT);
    pinMode(LED_BLUE, OUTPUT);
    
    pinMode(BUTTON_PIN, INPUT_PULLUP);
    
    digitalWrite(LED_RED, HIGH);
    digitalWrite(LED_GREEN, HIGH);
    digitalWrite(LED_BLUE, HIGH);

    os_init();

    LMIC_reset();
    LMIC_setClockError(MAX_CLOCK_ERROR * 1 / 100);
    LMIC_startJoining();
}

void loop()
{
    static int flag = false;
    bool newData = false;
    unsigned long age;
    os_runloop_once();

    while (Serial1.available())
    {
      // Feed TinyGPS with NMEA messages
      char c = Serial1.read();
      if (gps.encode(c)) newData = true;  // Set newData flag if a new GPS data is available
    }

    if (newData)
    {
      // If a new GPS data is available, update GPS position vars (flat, flon and age)
      gps.f_get_position(&flat, &flon, &age);
      if ( flat == TinyGPS::GPS_INVALID_F_ANGLE ) flat = DEFAULT_LAT;
      if ( flon == TinyGPS::GPS_INVALID_F_ANGLE ) flon = DEFAULT_LON;
      if ( (flat != 0) && (flon != 0) ) digitalWrite(LED_GREEN, LOW);
      newData = false;
    }
  
    if ( digitalRead(BUTTON_PIN) )
    {
      if ( flag ) {
        // If the Yah! push button is released (once)
        flag = false;
        digitalWrite(LED_GREEN, HIGH);
      }
    }
    else
    {
      if ( !flag ) {
        // If the Yah! push button is pressed (once)
        flag = true;
        digitalWrite(LED_GREEN, LOW);   
        do_send(NULL);
      }
    }
}


