# -*- coding: utf-8 -*-
"""
Created on Thu Feb 14 15:17:41 2019

@author: remi
"""

# python -m pip install --upgrade pip
# sudo python -m pip install grpcio
# sudo pip install requests

import requests
import json

#######################
# JWT Token : À créer dans l'interface web via internal/login
# Test connexion à l'API
######################

jwt2 = ("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJsb3JhLWFwcC1zZXJ2ZXIiL"
       "CJleHAiOjE1NTAyNDMwNDYsImlzcyI6ImxvcmEtYXBwLXNlcnZlciIsIm5iZiI6MTU1MDE"
       "1NjY0Niwic3ViIjoidXNlciIsInVzZXJuYW1lIjoicmJvdWxsZSJ9.Ch14zYMXSRDy_iu8"
       "J_6ZS5iA1Yfgp2dfO3xQWq7j6V4")

jwt = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdWQiOiJsb3JhLWFwcC1zZXJ2ZXIiLCJleHAiOjE1NTA2MTE4MjUsImlzcyI6ImxvcmEtYXBwLXNlcnZlciIsIm5iZiI6MTU1MDUyNTQyNSwic3ViIjoidXNlciIsInVzZXJuYW1lIjoicmJvdWxsZSJ9.Gk5X8vXj8qX-nP7pZ70Mt-7dkg6LUdzA4KfQ_ENyGSI"

auth_headers={'Grpc-Metadata-Authorization': jwt}

url = 'https://loraserver.tetaneutral.net/api/applications'

resp = requests.get(url, headers=auth_headers)
print resp
if resp.status_code != 200:
    print "Erreur de connection !"
else:
    print "OK !"

def add_device_to_application(appID, devEUI):
    url_device = 'https://loraserver.tetaneutral.net/api/devices'
    devProfileID = "25c08b1b-9f83-443b-b5a8-57982bf0ebb9"
    numero_du_Yah = devEUI[-4:]
    device_dic = {"device": {
            "applicationID": str(appID),
            "description": str("YahIUT"+numero_du_Yah),
            "devEUI": str(devEUI),
            "deviceProfileID": str(devProfileID),
            "name": str("YahIUT"+numero_du_Yah),
            "referenceAltitude": 0,
            "skipFCntCheck": True}
            }
    device_keys = {"deviceKeys": {
            "devEUI": str(devEUI),
            "nwkKey": "00000000000000001A81070000000200"}}
    body_device = json.dumps(device_dic)
    body_device_keys = json.dumps(device_keys)
    
    resp = requests.post(url_device,
                     data=body_device,
                     headers={'Grpc-Metadata-Authorization': jwt})
    print resp.status_code         
    url_device_keys = 'https://loraserver.tetaneutral.net/api/devices/{}/keys'.format(str(devEUI))
    resp = requests.post(url_device_keys, data=body_device_keys, headers=auth_headers)
    print resp.status_code

#add_device_to_application(appID=15,
#                          devEUI="1a810700000002fe",
#                          mwkey="00000000000000001A81070000000200")

def remove_device(devEUI):
    url = 'https://loraserver.tetaneutral.net/api/devices/{}'.format('str(devEUI)')
    resp = requests.delete(url, headers=auth_headers)
    
#liste_devEUI = ["1a81070000000{}".format(str(i)) for i in range(201,300)]
liste_devEUI = ["1a810700000002{}".format(hex(i)[2:].zfill(2)) for i in xrange(1,256)]

for devEUI in liste_devEUI:
    add_device_to_application(21, devEUI)


