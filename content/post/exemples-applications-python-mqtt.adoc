---
title: Un client MQTT en python
tags: ["MQTT", "python", "Paho"]
date: "2018-12-13"
---

:toc: left
:toclevels: 3
:data-uri:
:icons: font
:imagesdir: ./content/post/images/
:source-highlighter: highlightjs
:highlightjs-theme: solarized_dark

= Client MQTT en +python+

== Source complet

[source,python]
----
include::src/Python-MQTT-SimpleSub/mqtt-simple.py[]
----
